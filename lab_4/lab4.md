### Системные вызовы, процессы, компиляция программ в ОС Эльбрус

#### 1. Цель работы:
* Приобретение навыков по управлению процессами;
* Изучение системного вызова fork();
* Приобретение навыков написания и трансляции системного ПО на языке C;
* Изучение утилиты make и её использование для трансляции программ на языке C;
* Использование удалённых серверов для трансляции приложений.

#### 2. Выполнение работы:
Для начала в работе требуется определить домашний (начальный) каталог текущего пользователя.
>В ОС Эльбрус при входе в систему имеется возможность заходить как от имени суперпользователя root, так и от имени обычного пользователя (при условии, если обычный пользователь был создан при установке системы или же впоследствии добавлен в систему суперпользователем root).

В данном случае, вход в систему был выполнен от имени обычного пользователя user_9, который был создан на одном из этапов установки ОС. Поэтому домашний каталог здесь должен отобразиться для пользователя user_9.

Проведем проверку. Для этого отобразим содержимое переменной HOME:

![](pictures/home.PNG)

Рисунок 1 - Определение домашней папки текущего пользователя

Как видно на рисунке 1, в переменной HOME действительно содержится путь к домашнему каталогу пользователя user_9, как и должно быть. При переходе в домашнюю папку с помощью cd ~ тем более можно убедиться, что для текущего пользователя путь домашней папки определяется как /home/user_9.
>То есть, в ОС Эльбрус здесь не наблюдалось никаких отклонений.

Далее, в соответствии с заданием, в домашнем каталоге был создан каталог work, в который с помощью команды scp были скопированы файлы для работы с сервера лаборатории:

![](pictures/scp.PNG)

Рисунок 2 - Загруженные файлы для работы
>При этом в ОС Эльбрус также не наблюдалось проблем: не нужно было дополнительно устанавливать ПО для scp или ssh, запуск команды scp был осуществлен сразу.

После загрузки файлов был распакован архив source.tar.bz2 в каталог work, предварительно из которого все находящиеся в нем файлы были перемещены в другой каталог:

![](pictures/raspakovka.PNG)

Рисунок 3 - Распакованные файлы для работы
>При использовании команды tar вместе с указанными на рисунке 3 опциями в ОС Эльбрус проблем тоже не возникло.

Теперь можно было приступать к исправлению ошибок в заготовках исходных текстов программ.

Как видно на рисунке 3, в данной работе исходный текст программы хранится в следующих файлах: три файла исходного кода (lab4.c, pr_a.c, pr_b.c) и один заголовочный файл (lab4.h). Также есть файл с именем Makefile для утилиты make, в котором находится набор правил для выполнения трансляции.

Требующий исправлений первоначальный вид файла lab4.c:

![](pictures/isx1.PNG)

![](pictures/isx3.PNG)

Рисунок 4 - Исходный вид файла lab4.c

В данном файле, после объявления и инициализации переменной int b = 100; и до занесения результатов выполнения системного вызова `getpid()` в переменную pid, пропущен системный вызов `fork()` для создания нового процесса в операционной системе, так как, судя по приведенным комментариям в коде, после инициализации переменных a и b должен быть создан новый процесс, и далее должны работать псевдопараллельно два процесса.

Также был подправлен следующий комментарий:
>/\* Узнаем идентификаторы **текущего и** родительского процесса (в каждом из процессов) \*/

, так как далее в коде следуют системные вызовы для определения не только идентификатора родительского процесса, но и текущего с помощью`getpid()`.

Дополнительно была подправлена конструкция else if, чтобы в ней осуществлялось сравнение с 0 не переменной pid, а результата выполнения системного вызова `fork()`.

Полностью исправленный вариант файла lab4.c представлен ниже:

![](pictures/isx4.PNG)

![](pictures/isx5.PNG)

Рисунок 5 - Итоговый вид файла lab4.c

Что касается make-файла, в нем таились "невидимые" ошибки: вместо отступов с помощью пробела нужно было вставить отступы с помощью клавиши табуляции, иначе команды не будут работать:

![](pictures/isx6.PNG)

Рисунок 6 - Исправленный файл Makefile

Прежде чем переходить к трансляции и использованию утилиты make, была осуществлена проверка версии gcc в ОС Эльбрус:

![](pictures/vers.PNG)

Рисунок 9 - Установленная версия GCC в ОС Эльбрус

И уже потом была применена команда make без аргументов, чтобы выполнить трансляцию и компановку программы:

![](pictures/sborka.PNG)

Рисунок 10 - Выполнение команды make

На данном этапе получаем предупреждение, показанное на рисунке 10: `-Wimplicit-function-declaration`. Вообще вывод данного предупреждения обычно происходит в том случае, если функция используется до того, как определена. Поэтому был подправлен заголовочный файл:

![](pictures/ispr_lab4h.PNG)

Рисунок 11 - Исправленный файл lab4.h

После очередной сборки с помощью make, как видно на рисунке 12, предупреждений больше не появлялось. Также была выполнена команда make install для копирования lab4 в каталог bin:

![](pictures/ysp_sborka.PNG)

Рисунок 12 - Успешная сборка

Результат выполнения программы bin/lab4 показан на рисунке 13:

![](pictures/rez.PNG)

Рисунок 13 - Выполнение программы bin/lab4

Как можно увидеть, у одного из процессов идентификатор родительского процесса равен 5038, а у другого процесса этот идентификатор является его собственным PID. С переменными a и b каждый процесс работал отделньно, как видно по их значениям на скриншоте. Следовательно, программа была выполнена верно.

И уже после этого была выполнена команда make clean для удаления исполняемого и объектных файлов, не находящихся в каталоге bin:

![](pictures/clean.PNG)

Рисунок 14 - Выполнение команды make clean

Как видно, данная команда тоже выполнилась без ошибок.

#### 3. Вывод:
В данной работе была проведена проверка работы системных вызовов fork(), getpid() и getppid() в ОС Эльбрус версии 4.0. При работе с ними в данной ОС никаких отклонений замечено не было. То же касается и утилиты make.

Но, стоит отметить, что в ОС Эльбрус установлена не самая новая версия компилятора GCC - 5.5. Последняя версия GCC - 9.2, которая была выпущена 12 августа 2019.
